package edu.kau.fcit.cpit252;


import edu.kau.fcit.cpit252.observers.EmailObserver;
import edu.kau.fcit.cpit252.observers.Observer;
import edu.kau.fcit.cpit252.observers.SMSObserver;
import edu.kau.fcit.cpit252.observers.TwitterObserver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


public class ObserverTest {
    private final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errStream = new ByteArrayOutputStream();
    private final PrintStream sOut = System.out;
    private final PrintStream sErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outStream));
        System.setErr(new PrintStream(errStream));
    }

    @After
    public void restoreStreams() {
        System.setOut(sOut);
        System.setErr(sErr);
    }

    @Test
    public void testTwitterObserver(){
        String twHandle = "twitter_handle";
        Observer tw = new TwitterObserver(twHandle);
        assertEquals(tw.getRecipient(), twHandle);
        String message = "new message";
        tw.update(message);
        assertTrue(outStream.toString().contains(message));
    }

    @Test
    public void testSMSObserver(){
        String phoneNumber = "009661234567890";
        Observer sms = new SMSObserver(phoneNumber);
        assertEquals(sms.getRecipient(), phoneNumber);
        String message = "new message";
        sms.update(message);
        assertTrue(outStream.toString().contains(message));
    }

    @Test
    public void testEmailObserver(){
        String emailAddress = "noemail@noemail";
        Observer email = new EmailObserver(emailAddress);
        assertEquals(email.getRecipient(), emailAddress);
        String message = "new message";
        email.update(message);
        assertTrue(outStream.toString().contains(message));
    }



}
