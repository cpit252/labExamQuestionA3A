package edu.kau.fcit.cpit252;

import edu.kau.fcit.cpit252.observers.EmailObserver;
import edu.kau.fcit.cpit252.observers.Observer;
import edu.kau.fcit.cpit252.observers.SMSObserver;
import edu.kau.fcit.cpit252.observers.TwitterObserver;
import edu.kau.fcit.cpit252.subjects.PodcastSubject;
import edu.kau.fcit.cpit252.subjects.Subject;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        // The observers
        Observer[]observers = {
                new TwitterObserver("someTwitterHandle"),
                new SMSObserver("0096650000000"),
        new EmailObserver("noemail@noemail.gmail.com")};
        // The subject
        Subject s = new PodcastSubject();
        // subscribe to the subject
        for (Observer o : observers) {
            s.subscribe(o);
        }
        // broadcast the subject's message to all observers
        for (Observer o : observers) {
            s.notifyUpdate("New podcast episode released!");
        }
        // unsubscribe from the subject
        for (Observer o : observers) {
            s.unsubscribe(o);
        }
    }
}
