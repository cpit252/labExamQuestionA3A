package edu.kau.fcit.cpit252.observers;

public class SMSObserver extends Observer {

    public SMSObserver(String recipient){
        super.setRecipient(recipient);
    }

    @Override
    public void update(String message) {
        System.out.println("SMS Observer :: Recipient: @" + getRecipient() + "\nMessage: " + message);
    }
}
