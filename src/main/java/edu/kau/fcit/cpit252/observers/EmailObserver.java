package edu.kau.fcit.cpit252.observers;

import edu.kau.fcit.cpit252.utils.EmailSender;
import edu.kau.fcit.cpit252.utils.MissingRequiredPropetiesException;

public class EmailObserver extends Observer{

    public EmailObserver(String recipient){
        super.setRecipient(recipient);
    }
    @Override
    public void update(String message){
        try {
            EmailSender.send("New Podcast Episode", message, getRecipient());
            System.out.println("Email Observer :: Recipient: " + getRecipient() + "\nMessage: " + message);
       } catch (MissingRequiredPropetiesException e) {
            System.err.println(e.getMessage());
        }
    }
}
