### Lab exam question A3A:
A podcast studio can notify subscribers when it publishes a new podcast. Releasing a new podcast is what changes the state of the podcast studio, and it causes the subscribers to be notified via email, SMS, and twitter. Implement this scenario using the observer design pattern.

### Building, testing, running

```
mvn compile;
mvn test;
mvn compile exec:java -Dexec.mainClass="edu.kau.fcit.cpit252.App";
```


